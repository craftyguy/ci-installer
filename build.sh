#!/bin/bash

set -o errexit
set -o nounset
#set -o xtrace
set -o pipefail

DISK_FILE=ci_disk.qcow2
WORKING_DIR="$(mktemp -dt simple_ci_gateway_iso_builder.XXXXXXXXXX)"
OUTPUT_DIR=$(pwd)/output

iso() {
    ROOT="$(pwd)"/releng/airootfs

    FARM_NAME=${FARM_NAME?Farm name not supplied}
    GITLAB_REGISTRATION_TOKEN=${GITLAB_REGISTRATION_TOKEN?Gitlab registration token must be suppled}
    GITLAB_ACCESS_TOKEN=${GITLAB_ACCESS_TOKEN?Gitlab access token must be suppled}

    rm -rf "$ROOT"/etc/simple_ci_gateway/
    mkdir -v "$ROOT"/etc/simple_ci_gateway/
    cat <<EOF >"$ROOT"/etc/simple_ci_gateway/private.env
FARM_NAME=$FARM_NAME
GITLAB_REGISTRATION_TOKEN=$GITLAB_REGISTRATION_TOKEN
GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN
EOF

    rm -rf "$ROOT"/root/.ssh/
    rm -rf $OUTPUT_DIR
    mkdir -pv $OUTPUT_DIR

    mkdir "$ROOT"/root/.ssh/

    keyname=${OUTPUT_DIR}/${FARM_NAME}-ssh_key-$(date +%Y.%m.%d)
    ssh-keygen -C "simple ci gateway admin" -f $keyname -P ""
    cp ${keyname}.pub "$ROOT"/root/.ssh/authorized_keys

    echo "root rights required for ISO build..."

    sudo whoami
    sudo mkarchiso -v -w "$WORKING_DIR/iso" -o "$WORKING_DIR/build" releng
    cp -v "$WORKING_DIR"/build/simple_ci_gateway-*-x86_64.iso ${OUTPUT_DIR}/${FARM_NAME}-$(date +%Y.%m.%d)-x86_64-installer.iso
    ( cd $OUTPUT_DIR && sha256sum -b *.iso > SHA256SUMS.txt )

    sudo rm -rf "$WORKING_DIR"
    echo "ISO generated into ${OUTPUT_DIR}"
}

test_installer() {
    [ -f $DISK_FILE ] || qemu-img create -f qcow2 $DISK_FILE 100G

    local ISO=${1?You must provide an ISO image to test}

    qemu-system-x86_64 \
	-hda $DISK_FILE \
	-cdrom "$ISO" \
	-boot d \
        -device virtio-net-pci,romfile=,netdev=net0 \
	-device virtio-net-pci,romfile=,netdev=net1 \
	`# Simulate the plugged in "upstream" cable with user-mode networking` \
	-netdev user,id=net0,hostfwd=tcp::60022-:22 \
	`# And now the unplugged one with, with TAP networks` \
	-netdev tap,id=net1,ifname=tap0,script=no,downscript=no \
	$BOOT_MODE \
	-m 4G \
	-display sdl \
	-enable-kvm \
	-serial stdio
    reset  # the serial port on stdout needs cleaning up
}

test_gateway() {
    [ -f $DISK_FILE ] || ( echo "no disk file, not installed!" && exit 1 )
    qemu-system-x86_64 \
	-hda $DISK_FILE \
	-boot c \
        -device virtio-net-pci,romfile=,netdev=net0 \
	-device virtio-net-pci,romfile=,netdev=net1 \
	`# Simulate the plugged in "upstream" cable with user-mode networking` \
	-netdev user,id=net0,hostfwd=tcp::60022-:22 \
	`# And now the unplugged one with, with TAP networks` \
	-netdev tap,id=net1,ifname=tap0,script=no,downscript=no \
	$BOOT_MODE \
	-m 4G \
	-display sdl \
	-enable-kvm \
	-serial stdio
    reset  # the serial port on stdout needs cleaning up
}

test_dut() {
    [ -f dut_disk.qcow2 ] || qemu-img create -f qcow2 dut_disk.qcow2 32G
    qemu-system-x86_64 \
	-hda dut_disk.qcow2 \
	-boot n \
        -device virtio-net-pci,romfile=,netdev=net0 \
	-netdev tap,id=net0,ifname=tap1,script=no,downscript=no \
	$BOOT_MODE \
	-m 1G \
	-display sdl \
	-enable-kvm \
	-serial stdio
    reset  # the serial port on stdout needs cleaning up
}

dohelp() {
    cat <<EOF
$0 build_iso

  Construct an ISO image

  The output files will be placed in the ./output directory by
  default.

  The environment must contain the following,

    FARM_NAME - unique name for your farm
    GITLAB_REGISTRATION_TOKEN - registration token for runners
    GITLAB_ACCESS_TOKEN - gitlab api access token

$0 [--uefi | --bios] test_installer <installer_iso>


$0 [--uefi | --bios] test_gateway
EOF
}

set +o nounset
while test -n "$1"; do
    case "$1" in
	--help*|-h*)
	    dohelp
	    exit 0
	    ;;
	--uefi)
	    cp /usr/share/edk2-ovmf/x64/OVMF_VARS.fd .
	    BOOT_MODE="-drive if=pflash,format=raw,unit=0,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd,readonly -drive if=pflash,format=raw,unit=1,file=OVMF_VARS.fd -global driver=cfi.pflash01,property=secure,value=off"	      
	    ;;
	--bios)
	    BOOT_MODE=""
	    ;;
	*)
	    cmd=$1
	    shift
	    break
	    ;;
    esac
    shift
done
set -o nounset

# Default to BIOS
[ -z "${BOOT_MODE+x}" ] && BOOT_MODE=""
[ -z "${cmd+x}" ] && dohelp && exit 1

eval "$cmd" "$@"
