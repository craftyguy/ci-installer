#!/bin/bash
# -*- mode: shell-script -*-

# Reference the valve-infra master repo in the future...

# There are many cases where it is known the argument will not
# container problematic IFS characters, for which double quoting them
# makes the script very ugly, since exceptions are common to the rule.
# shellcheck disable=SC2086

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# Reference the valve-infra master repo in the future...
ENTRYPOINT='https://druid.fish/public/ci_entrypoint.sh'

umount -R /mnt || true

find_disk() {
    IFS=$'\n'
    declare -A candidates
    for dev in $(lsblk --list --noheadings --nodeps --output NAME); do
	echo -n "Checking $dev..."
	if [[ $dev != nvme* ]] && [[ $dev != sd* ]]; then
	    echo " not a relevant disk type, skip"
	    continue
	fi

	if grep -q /dev/$dev /etc/mtab ; then
	    echo " already mounted, skip"
	    continue
	fi

	size=$(lsblk  -b /dev/$dev --list --nodeps --noheadings --output SIZE)
	candidates[$dev]=$size
	echo " in consideration"
    done

    num_candidates=${#candidates[@]}
    if [ $num_candidates -eq 0 ];  then
	echo "No suitable disk found. Abort!"
	exit 1
    else
	device_gbs=0
	for dev in ${!candidates[*]}; do
	    gbs=$(( ${candidates[$dev]} / (1024*1024*1024) ))
	    echo "$dev -- $gbs GiB"
	    if [ $gbs -gt $device_gbs ]; then
		device=/dev/$dev
		device_gbs=$gbs
	    fi
	done
    fi

    echo "Found a suitable block device: $device"
    if [[ $device != /dev/nvme* ]]; then
	efi_dev=${device}2
	root_dev=${device}3
	permanent_dev=${device}4
	tmp_dev=${device}5
    else
	efi_dev=${device}p2
	root_dev=${device}p3
	permanent_dev=${device}p4
	tmp_dev=${device}p5
    fi
    unset IFS
}

setup_disk() {
    local boot_num=1
    local efi_num=2

    # Partition the drive
    echo "Formating the device $device"

    parted -s "$device" -- mklabel gpt \
	   mkpart BOOT 2048s 2MiB \
	   set $boot_num bios_grub on \
	   mkpart EFI fat32 2MiB 512MiB \
	   set $efi_num esp on \
	   mkpart ROOT btrfs 512MiB 20GiB \
	   mkpart PERM btrfs 20GiB 21GiB \
	   mkpart TMP btrfs 21GiB 100%

    mkfs.fat -F32 -n EFI ${efi_dev}
    mkfs.btrfs -f --label ROOT ${root_dev}
    mkfs.btrfs -f --label PERM ${permanent_dev}
    mkfs.btrfs -f --label TEMP ${tmp_dev}
}

mount_disk() {
    # Make sure the kernel knows about all the devices before mounting
    btrfs device scan

    # Mount the drives
    mount ${root_dev} /mnt
    mkdir -p /mnt/mnt/{permanent,tmp}
    mount ${permanent_dev} /mnt/mnt/permanent
    mount ${tmp_dev} /mnt/mnt/tmp
}

setup_bootloader() {
    arch-chroot /mnt grub-install --target=i386-pc ${device}
    if [ -d /sys/firmware/efi ]; then
	mkdir -p /mnt/efi
	mount ${efi_dev} /mnt/efi
	arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
	cat <<GRUBCFG > /mnt/efi/EFI/GRUB/grub.cfg
search --label ROOT --set prefix
configfile (\$prefix)/boot/grub/grub.cfg
GRUBCFG
	umount /mnt/efi
	rm -rf /mnt/efi
    fi
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
}

bootstrap_arch() {
    pacstrap /mnt \
	     base \
	     bash \
	     dhcpcd \
	     `# Todo: move to podman` \
	     docker \
	     efibootmgr \
	     git \
	     grub \
	     jq \
	     linux \
	     linux-firmware \
	     nano \
	     openssh \
	     python \
	     python-netifaces \
	     python-pip \
	     tmux \
	     vim

    # Sometimes the UUID detected by genfstab in the installer
    # environment is different to the UUID seen when booting the
    # installed system... Noticed this under QEMU. Somewhat
    # paradoxically, labels seem more robust to such changes, and
    # should be find for our use-cases, since collisions are very
    # unlikely.
    genfstab -L /mnt > /mnt/etc/fstab

    # journald
    arch-chroot /mnt mkdir -p /var/log/journal /mnt/tmp/journal
    echo '/mnt/tmp/journal /var/log/journal none defaults,bind 0 0' >> /mnt/etc/fstab

    arch-chroot /mnt ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    arch-chroot /mnt hwclock --systohc

    sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /mnt/etc/locale.gen
    arch-chroot /mnt locale-gen -a
    cat <<EOF > /mnt/etc/locale.conf
# tmuxp gets very upset if the locale is not UTF-8.
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8
EOF

    # Todo: setup a non-priv user?
    echo 'root:x:0:0:root:/root:/bin/bash' >/mnt/etc/passwd
    echo 'root::::::::' >/mnt/etc/shadow
}

id_network_devices() {
    # The operator must ensure one of the two NICs is plugged to an
    # upstream DHCP server. We exploit this assumption by then finding
    # the routable interface and assigning it the consistent name of
    # "public", for reference by the CI system. The other, degraded,
    # link is assumed the "private" link, where test devices reside.
    num_enfaces=$(networkctl --no-pager --no-legend | grep -cE ' [0-9] en.*')
    [ $num_enfaces == 2 ] || ( echo "expected 2 ethernet network interfaces" && exit 1 )

    public=$(networkctl --no-pager --no-legend | grep -E ' [0-9] en.*' | awk '/routable/ { print $2 }')
    private=$(networkctl --no-pager --no-legend | grep -E ' [0-9] en.*' | awk '/degraded/ { print $2 }')
    
    [ -z "$public" ] && ( echo "no public interface detected" && exit 1 )
    [ -z "$private" ] && ( echo "no private interface detected" && exit 1 )

    echo "public iface: $public"
    echo "private iface: $private"
}    

setup_network() {
    echo 'gateway'  > /mnt/etc/hostname

    cat <<EOF > /mnt/etc/hosts
127.0.0.1	localhost
EOF

    # get the mac address, setup systemd network files to configure the naming above
    #ip addr add 10.42.0.1/24 dev private || true
    #ip link set private up
    public_mac=$(cat /sys/class/net/$public/address)
    private_mac=$(cat /sys/class/net/$private/address)

    cat <<EOF > /mnt/etc/systemd/network/00-public.link
[Match]
MACAddress=$public_mac

[Link]
NamePolicy=
Name=public
EOF

    cat <<EOF > /mnt/etc/systemd/network/00-public.network
[Match]
Name=public

[Network]
DHCP=yes
EOF

    cat <<EOF > /mnt/etc/systemd/network/10-private.link
[Match]
MACAddress=$private_mac

[Link]
NamePolicy=
Name=private
EOF

    cat <<EOF > /mnt/etc/systemd/network/10-private.network
[Match]
Name=private

[Network]
DHCP=no
; Should probably parameterize this, for VPN use-cases...
Address=10.42.0.1/24
; Todo: instead of running our own NTP server, use timesyncd
; NTP=
; Enable routing for the DUTs
IPMasquerade=ipv4
EOF

    arch-chroot /mnt systemctl enable systemd-networkd.service

    cat <<EOF >/mnt/etc/resolv.conf
nameserver 1.1.1.1
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

}

setup_services() {
    # secrets / site-configuration
    cp -rv /etc/simple_ci_gateway /mnt/etc/

    # autologin
    sed -i '
s|#NAutoVTs=6|NAutoVTs=2|
s|#KillUserProcesses=no|KillUserProcesses=yes|
' /mnt/etc/systemd/logind.conf

    mkdir -pv /mnt/etc/systemd/system/getty@tty1.service.d
    cat <<EOF >/mnt/etc/systemd/system/getty@tty1.service.d/override.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin root --noclear %I 38400 linux
EOF

    # docker
    mkdir -pv /mnt/etc/docker
    cat <<EOF >/mnt/etc/docker/daemon.json
{
    "data-root": "/mnt/tmp/docker/data_root"
}
EOF
    arch-chroot /mnt systemctl enable docker.service

    # ssh
    cat<<EOF >>/mnt/etc/ssh/sshd_config
Match Address 10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
    PasswordAuthentication no
EOF
    rsync -av .ssh /mnt/root/
    arch-chroot /mnt systemctl enable sshd.service

    cp -a /usr/local/bin/selftest /mnt/usr/local/bin/selftest
    cat <<EOF >/mnt/usr/local/bin/gateway_status
#!/bin/bash

ip -4 addr list dev public
ip -4 addr list dev private
df -h
systemctl status infra.service
EOF
    chmod +x /mnt/usr/local/bin/gateway_status

    # ci service
    rsync -av /usr/local/bin/ci_entrypoint.sh /mnt/usr/local/bin/ci_entrypoint.sh
    cat <<EOF >/mnt/etc/systemd/system/infra.service
[Unit]
Description=Valve Gateway service
Requires=docker.service
After=docker.service
Requires=network-online.target
After=network-online.target

[Service]
Type=simple
Environment=HOME=/root
WorkingDirectory=/root
Restart=on-failure
RestartSec=10s
#ExecStartPre=curl -o ci_entrypoint.sh $ENTRYPOINT
ExecStart=/usr/local/bin/ci_entrypoint.sh

[Install]
WantedBy=multi-user.target
EOF
    arch-chroot /mnt systemctl enable infra.service

    # tmux dashboard
    # The Arch package was broken at the time of this choice.
    arch-chroot /mnt pip install tmuxp
    rm -rf /mnt/root/.tmuxp
    mkdir /mnt/root/.tmuxp
    cat <<EOF >/mnt/root/.tmuxp/dashboard.yml
session_name: dashboard
windows:
- window_name: main
  layout: tiled
  panes:
    - watch /usr/local/bin/selftest
    - watch /usr/local/bin/gateway_status
    - journalctl -fu infra
- window_name: executor
  layout: tiled
  panes:
    - watch "curl -s -o- http://10.42.0.1:8003/api/v1/machines | jq -r  '.[] | to_entries | .[] | [ .key, .value.state, (.value.tags | sort | join(\"|\")), (if .value.ready_for_service then \"ONLINE\" else \"OFFLINE\" end) ] | @tsv'"
EOF

    cat <<EOF >/mnt/root/.bash_login
set +o nounset
if [ -z "\$SSH_CONNECTION" ]; then
   set -o nounset
   [ -z "\${TMUX+z}" ] && tmuxp load dashboard

   # If you exit here, the service will auto-restart thanks to the login script trick.
   # exit 0
   # For now, fall thru and open the shell.
   exec bash
fi

# present the shell if coming from SSH
export TERM=xterm

EOF
}

cleanup() {
    umount -R /mnt || true
}
trap cleanup SIGTERM SIGINT ERR

find_disk
id_network_devices
setup_disk
mount_disk
bootstrap_arch
setup_bootloader
setup_network
setup_services
cp installer.log /mnt/
cleanup

# Todo: set the bios to boot from disk next? possible?
#   You can mess with EFI flash if UEFI is chosen, for now it's BIOS/GPT,
#   not sure about the BIOS. Ask the operator!
echo "Valve CI gateway was installed to ${device}. Reboot from the disk!"
