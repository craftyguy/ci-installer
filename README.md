Construct the ISO file for a specific site,

	env FARM_NAME=<farm_name> \
		GITLAB_REGISTRATION_TOKEN=<registration_token> \
		GITLAB_ACCESS_TOKEN=<access_token> \
		./build.sh iso

This will produce an ISO in the `./output` directory with a name like `<farm_name>-$(date +%Y.%m.%d)-x86_64.iso`.

Test the ISO installer created above using QEMU, this will create a disk file which can be validated later.

	./build.sh test_installer ./output/<farm_name>-$(date +%Y.%m.%d)-x86_64.iso

After installation, test the resulting gateway disk iamge,

	./build.sh test_gateway

By default, the installer and disk will boot in legacy BIOS mode. You can make this explicit with `./build.sh --bios ...`, or use a UEFI flash with `./build.sh --uefi ...`

You can connect to either the live CD or the installed system thusly,

	ssh -i ./output/gateway_ssh_key root@localhost -p 60022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

